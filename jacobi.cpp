/**
 * @file    jacobi.cpp
 * @author  Patrick Flick <patrick.flick@gmail.com>
 * @brief   Implements matrix vector multiplication and Jacobi's method.
 *
 * Copyright (c) 2014 Georgia Institute of Technology. All Rights Reserved.
 */
#include "jacobi.h"

/*
 * TODO: Implement your solutions here
 */

// my implementation:
#include <iostream>
#include <math.h>
#include <stdlib.h>

// Calculates y = A*x for a square n-by-n matrix A, and n-dimensional vectors x
// and y
void matrix_vector_mult(const int n, const double* A, const double* x, double* y)
{
    // TODO
    for (int i = 0; i < n; i++) {
        double sum = 0;
        for (int j = 0; j < n; j++) {
            sum += *(A + i * n + j) * *(x + j);
        }
        y[i] = sum;
    }
    return;
}

// Calculates y = A*x for a n-by-m matrix A, a m-dimensional vector x
// and a n-dimensional vector y
void matrix_vector_mult(const int n, const int m, const double* A, const double* x, double* y)
{
    // TODO
    for (int i = 0; i < n; i++) {
        double sum = 0;
        for (int j = 0; j < m; j++) {
            sum += *(A + i * m + j) * *(x + j);
        }
        y[i] = sum;
    }
    return;
}

// implements the sequential jacobi method
void jacobi(const int n, double* A, double* b, double* x, int max_iter, double l2_termination)
{
    // TODO
    int ites = 0;
    double* D = (double*) malloc(sizeof(double) * n);
    for (int i = 0; i < n; i++) {
        *(D + i) = *(A + i * n + i);
    }
    while (ites < max_iter) {
        double* y = (double*) malloc(sizeof(double) * n);
        matrix_vector_mult(n, A, x, y);
        
        double norm = 0;
        for (int i = 0; i < n; i++) {
            norm += (*(b + i) - *(y + i)) * (*(b + i) - *(y + i));
        }
        if (norm < l2_termination * l2_termination) {
            break;
        }
        
        for (int i = 0; i < n; i++) {
            *(x + i) = (*(b + i) - *(y + i) + *(D + i) * *(x + i)) / *(D + i);
        }
        //matrix_vector_mult(n, A, x, y);
        free(y);
        ites++;
    }
    free(D);
    return;
}
