/**
 * @file    mpi_jacobi.cpp
 * @author  Patrick Flick <patrick.flick@gmail.com>
 * @brief   Implements MPI functions for distributing vectors and matrixes,
 *          parallel distributed matrix-vector multiplication and Jacobi's
 *          method.
 *
 * Copyright (c) 2014 Georgia Institute of Technology. All Rights Reserved.
 */

#include "mpi_jacobi.h"
#include "jacobi.h"
#include "utils.h"

#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <vector>

/*
 * TODO: Implement your solutions here
 */


void distribute_vector(const int n, double* input_vector, double** local_vector, MPI_Comm comm)
{
    // TODO
    int rank00;
    int coords[2] = {0, 0};
    MPI_Cart_rank(comm, coords, &rank00);

    int p;
    MPI_Comm_size(comm, &p);
    int q = (int) sqrt(p);

    int dim[2];
    int periods[2];
    int mycoords[2];
    MPI_Cart_get(comm, 2, dim, periods, mycoords);
    
    
    int local_size;
    int* sendcounts = NULL;
    int* displs = NULL;
    if (mycoords[1] == 0) {
        if (mycoords[0] == 0) {
            sendcounts = (int*) malloc(sizeof(int) * p);
            displs = (int*) malloc(sizeof(int) * p);
            int sum = 0;
            for (int i = 0; i < p; i++) {
                int tempcoords[2];
                MPI_Cart_coords(comm, i, q, tempcoords);
                if (tempcoords[1] == 0) {
                    sendcounts[i] = block_decompose(n, dim[0], tempcoords[0]);
                    displs[i] = sum;
                    sum += sendcounts[i];
                }
                else {
                    sendcounts[i] = 0;
                    displs[i] = sum;
                }
            }
        }
        local_size = block_decompose_by_dim(n, comm, 0);
        (*local_vector) = (double*) malloc(sizeof(double) * local_size);
        
    }
    else {
        local_size = 0;
        (*local_vector) = (double*) malloc(sizeof(double) * 1);
    }
    MPI_Scatterv(input_vector, sendcounts, displs, MPI_DOUBLE, *local_vector, local_size, MPI_DOUBLE, rank00, comm);

    free(sendcounts);
    free(displs);
    return;
}


// gather the local vector distributed among (i,0) to the processor (0,0)
void gather_vector(const int n, double* local_vector, double* output_vector, MPI_Comm comm)
{
    // TODO
    int rank00;
    int coords[2] = {0, 0};
    MPI_Cart_rank(comm, coords, &rank00);

    int p;
    MPI_Comm_size(comm, &p);
    int q = (int) sqrt(p);

    int dim[2];
    int periods[2];
    int mycoords[2];
    MPI_Cart_get(comm, 2, dim, periods, mycoords);

    
    int local_size = 0;
    int* recvcounts = NULL;
    int* displs = NULL;
    if (mycoords[1] == 0) {
        if (mycoords[0] == 0) {
            recvcounts = (int*) malloc(sizeof(int) * p);
            displs = (int*) malloc(sizeof(int) * p);
            int sum = 0;
            for (int i = 0; i < p; i++) {
                int tempcoords[2];
                MPI_Cart_coords(comm, i, q, tempcoords);
                if (tempcoords[1] == 0) {
                    recvcounts[i] = block_decompose(n, dim[0], tempcoords[0]);
                    displs[i] = sum;
                    sum += recvcounts[i];
                }
                else {
                    recvcounts[i] = 0;
                    displs[i] = sum;
                }
            }
        }
        local_size = block_decompose_by_dim(n, comm, 0);
    }

    //output_vector = (double*) malloc(sizeof(double) * n);
    MPI_Gatherv(local_vector, local_size, MPI_DOUBLE, output_vector, recvcounts, displs, MPI_DOUBLE, rank00, comm);

    free(recvcounts);
    free(displs);
    //printf("[%d, %d] first element: %f", mycoords[0], mycoords[1], *(output_vector + 2));
    return;
}

void distribute_matrix(const int n, double* input_matrix, double** local_matrix, MPI_Comm comm)
{
    // TODO
    int rank;
    MPI_Comm_rank(comm, &rank);

    int rank00;
    int coords[2] = {0, 0};
    MPI_Cart_rank(comm, coords, &rank00);

    int p;
    MPI_Comm_size(comm, &p);
    int q = (int) sqrt(p);

    int dim[2];
    int periods[2];
    int mycoords[2];
    MPI_Cart_get(comm, 2, dim, periods, mycoords);

    double* local_matrix_1_temp = NULL;
    double** local_matrix_1 = &local_matrix_1_temp;
    int local_size;
    int* sendcounts = NULL;
    int* displs = NULL;
    if (mycoords[1] == 0) {
        if (mycoords[0] == 0) {
            sendcounts = (int*) malloc(sizeof(int) * p);
            displs = (int*) malloc(sizeof(int) * p);
            int sum = 0;
            for (int i = 0; i < p; i++) {
                int tempcoords[2];
                MPI_Cart_coords(comm, i, q, tempcoords);
                if (tempcoords[1] == 0) {
                    sendcounts[i] = block_decompose(n, dim[0], tempcoords[0]) * n;
                    displs[i] = sum;
                    sum += sendcounts[i];
                }
                else {
                    sendcounts[i] = 0;
                    displs[i] = sum;
                }
            }
        }
        local_size = block_decompose_by_dim(n, comm, 0) * n;
        (*local_matrix_1) = (double*) malloc(sizeof(double) * local_size);
        
    }
    else {
        local_size = 0;
        (*local_matrix_1) = (double*) malloc(sizeof(double) * 1);
    }
    MPI_Scatterv(input_matrix, sendcounts, displs, MPI_DOUBLE, *local_matrix_1, local_size, MPI_DOUBLE, rank00, comm);

    //printf("\nseperation, [%d, %d], next number: %f\n", mycoords[0], mycoords[1], *(*local_matrix_1 + 5));
    int rows = block_decompose_by_dim(n, comm, 0);
    int cols = block_decompose_by_dim(n, comm, 1);
    (*local_matrix) = (double*) malloc(sizeof(double) * rows * cols);

    sendcounts = (int*) malloc(sizeof(int) * dim[1]);
    displs = (int*) malloc(sizeof(int) * dim[1]);

    *(sendcounts) = block_decompose(n, dim[1], 0);
    *(displs) = 0;
    for (int i = 1; i < dim[1]; i++) {
        *(sendcounts + i) = block_decompose(n, dim[1], i);
        *(displs + i) = *(displs + i - 1) + *(sendcounts + i - 1);
    }

    MPI_Comm row_comm;
    MPI_Comm_split(comm, mycoords[0], mycoords[1], &row_comm);
    for(int i = 0; i < rows; i++)
       MPI_Scatterv((*local_matrix_1 + i*n), sendcounts, displs, MPI_DOUBLE, (*local_matrix + i*cols), cols, MPI_DOUBLE, 0, row_comm);
    
    //printf("[%d, %d], test: %f", mycoords[0], mycoords[1], *(*local_matrix + 1));
    free(sendcounts);
    free(displs);
    free(local_matrix_1);
    return;
}


void transpose_bcast_vector(const int n, double* col_vector, double* row_vector, MPI_Comm comm)
{
    // TODO
    int p;
    MPI_Comm_size(comm, &p);
    int q = (int) sqrt(p);

    int dim[2];
    int periods[2];
    int mycoords[2];
    MPI_Cart_get(comm, 2, dim, periods, mycoords);
    int rank;
    MPI_Comm_rank(comm, &rank);

    int rows = block_decompose_by_dim(n, comm, 0);
    int cols = block_decompose_by_dim(n, comm, 1);

    MPI_Request req;
    MPI_Status stat;

    //printf("rows2 = %d", rows);
    //row_vector = (double*) malloc(sizeof(double) * rows2);
    //printf("[%d, %d] srcrank = %d", mycoords[0], mycoords[1], srcrank);
    
    bool hasExchange = false;
    if (mycoords[0] == 0 && mycoords[1] == 0) {
        for (int i = 0; i < cols; i++) {
            *(row_vector + i) = *(col_vector + i);
        }
    }
    if (mycoords[0] == mycoords[1] && mycoords[0] != 0) {
        hasExchange = true;
        int srcrank;
        int srccords[2] = {mycoords[1], 0};
        MPI_Cart_rank(comm, srccords, &srcrank);
        //printf("[%d, %d], srcrank = %d", mycoords[0], mycoords[1], srcrank);
        MPI_Irecv(row_vector, cols, MPI_DOUBLE, srcrank, MPI_ANY_TAG, comm, &req);
    }

    if (mycoords[1] == 0 && mycoords[0] != 0) {
        int desrank;
        int descoords[2] = {mycoords[0], mycoords[0]};
        MPI_Cart_rank(comm, descoords, &desrank);
        //printf("[%d, %d], desrank = %d", mycoords[0], mycoords[1], desrank);
        MPI_Send(col_vector, rows, MPI_DOUBLE, desrank, 1, comm);
    }
    if (hasExchange) {
        MPI_Wait(&req, &stat);
    }
    

    MPI_Comm col_comm;
    MPI_Comm_split(comm, mycoords[1], rank, &col_comm);
    MPI_Bcast(row_vector, cols, MPI_DOUBLE, mycoords[1], col_comm);

    //printf("[%d, %d] first element: %f", mycoords[0], mycoords[1], *(row_vector + 1));
}


void distributed_matrix_vector_mult(const int n, double* local_A, double* local_x, double* local_y, MPI_Comm comm)
{
    // TODO
    
    int dim[2];
    int periods[2];
    int mycoords[2];
    MPI_Cart_get(comm, 2, dim, periods, mycoords);
    int rank;
    MPI_Comm_rank(comm, &rank);

    int rows = block_decompose_by_dim(n, comm, 0);
    int cols = block_decompose_by_dim(n, comm, 1);

    double* local_x_part;
    local_x_part = (double*) malloc(sizeof(double) * cols);
    transpose_bcast_vector(n, local_x, local_x_part, comm);

    double* local_r;
    local_r = (double*) malloc(sizeof(double) * rows);

    for (int i = 0; i < rows; i++) {
    	double sum = 0;
    	for (int j = 0; j < cols; j++) {
            sum += (*(local_x_part + j)) * (*(local_A + i * cols + j));
    	}
        *(local_r + i) = sum;
    }

    MPI_Comm row_comm;
    MPI_Comm_split(comm, mycoords[0], rank, &row_comm);
    MPI_Reduce(local_r, local_y, rows, MPI_DOUBLE, MPI_SUM, 0, row_comm);

    free(local_x_part);
    free(local_r);
    //printf("[%d, %d] first element = %f", mycoords[0], mycoords[1], *(local_y + 1));
}

// Solves Ax = b using the iterative jacobi method
void distributed_jacobi(const int n, double* local_A, double* local_b, double* local_x,
                MPI_Comm comm, int max_iter, double l2_termination)
{
    // TODO
    int rank00;
    int coords[2] = {0, 0};
    MPI_Cart_rank(comm, coords, &rank00);

    int p;
    MPI_Comm_size(comm, &p);
    int q = (int) sqrt(p);

    int dim[2];
    int periods[2];
    int mycoords[2];
    MPI_Cart_get(comm, 2, dim, periods, mycoords);

    int rows = block_decompose_by_dim(n, comm, 0);
    int cols = block_decompose_by_dim(n, comm, 1);

    double* local_D = (double*) malloc(sizeof(double) * rows);
    //double* local_R = (double*) malloc(sizeof(double) * rows * cols);

    MPI_Request req;
    MPI_Status stat;
    bool hasExchange = false;
    if (mycoords[1] == 0 && mycoords[0] != 0) {
        hasExchange = true;
        int srcrank;
        int srccoords[2] = {mycoords[0], mycoords[0]};
        MPI_Cart_rank(comm, srccoords, &srcrank);
        MPI_Irecv(local_D, rows, MPI_DOUBLE, srcrank, 1, comm, &req);
    }

    if (mycoords[0] == mycoords[1]) {
        for (int i = 0; i < rows; i++) {
            *(local_D + i) = *(local_A + i * cols + i);
        }
        if (mycoords[0] != 0) {
            int desrank;
            int descoords[2] = {mycoords[0], 0};
            MPI_Cart_rank(comm, descoords, &desrank);
            MPI_Send(local_D, rows, MPI_DOUBLE, desrank, 1, comm);
        }
    }

    if (hasExchange) {
        MPI_Wait(&req, &stat);
    }
    //printf("[%d, %d] local_D=[%f,%f]\n", mycoords[0], mycoords[1], *(local_D), *(local_D + 1));
    
    int ites = 0;
    for (int i = 0; i < rows; i++) {
        *(local_x + i) = 0;
    }
    while (ites < max_iter) {
        double* local_y = (double*) malloc(sizeof(double) * rows);
        distributed_matrix_vector_mult(n, local_A, local_x, local_y, comm);

        double norm = 0;
        if (mycoords[1] == 0) {
            for (int i = 0; i < rows; i++) {
                norm += (*(local_b + i) - *(local_y + i)) * (*(local_b + i) - *(local_y + i));
            }
        }
        double total_norm = 0;
        MPI_Allreduce(&norm, &total_norm, 1, MPI_DOUBLE, MPI_SUM, comm);
        if (total_norm < l2_termination * l2_termination) {
            break;
        }

        if (mycoords[1] == 0) {
            for (int i = 0; i < rows; i++) {
                *(local_x + i) = (-*(local_y + i) + *(local_b + i) + *(local_D + i) * *(local_x + i)) / *(local_D + i);
            }
        }
        free(local_y);
        ites++;
    }
    return;
}


// wraps the distributed matrix vector multiplication
void mpi_matrix_vector_mult(const int n, double* A,
                            double* x, double* y, MPI_Comm comm)
{
    // distribute the array onto local processors!
    double* local_A = NULL;
    double* local_x = NULL;
    distribute_matrix(n, &A[0], &local_A, comm);
    distribute_vector(n, &x[0], &local_x, comm);

    
    // allocate local result space
    double* local_y = new double[block_decompose_by_dim(n, comm, 0)];
    distributed_matrix_vector_mult(n, local_A, local_x, local_y, comm);

    // gather results back to rank 0
    gather_vector(n, local_y, y, comm);

    //printf("[1]: %f\n[2]: %f\n[3]: %f\n[4]:%f\n", *y, *(y+1), *(y+2), *(y+3));
}

// wraps the distributed jacobi function
void mpi_jacobi(const int n, double* A, double* b, double* x, MPI_Comm comm,
                int max_iter, double l2_termination)
{
    // distribute the array onto local processors!
    double* local_A = NULL;
    double* local_b = NULL;
    distribute_matrix(n, &A[0], &local_A, comm);
    distribute_vector(n, &b[0], &local_b, comm);

    // allocate local result space
    double* local_x = new double[block_decompose_by_dim(n, comm, 0)];
    distributed_jacobi(n, local_A, local_b, local_x, comm, max_iter, l2_termination);

    // gather results back to rank 0
    gather_vector(n, local_x, x, comm);

}
